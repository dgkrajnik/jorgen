FROM openjdk:8-alpine

RUN apk update && apk add git && rm -r /var/cache

RUN mkdir -p /opt
WORKDIR /opt
ARG JORGEN_VER=unknown
RUN git clone https://gitlab.com/dgkrajnik/jorgen.git
WORKDIR /opt/jorgen

COPY application.conf /opt/jorgen/application.conf

RUN chmod +x ./gradlew && ./gradlew jar && mv build/libs/jorgen-1.0-SNAPSHOT.jar . && ./gradlew clean

RUN mv initdb.bat initdb.sh && chmod +x initdb.sh
RUN ./initdb.sh

ENTRYPOINT ["java", "-jar", "jorgen-1.0-SNAPSHOT.jar"]
