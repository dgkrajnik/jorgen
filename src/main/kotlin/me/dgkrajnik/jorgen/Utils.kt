package me.dgkrajnik.jorgen

import net.dv8tion.jda.core.entities.User
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger
import java.time.Duration
import java.time.Instant
import java.time.Period
import kotlin.coroutines.experimental.buildSequence

fun <R : Any> R.logger(): Lazy<Logger> {
    return lazy { LogManager.getLogger(this.javaClass.name) }
}

fun String.wordSeq(): Sequence<String> {
    val t = this
    return buildSequence {
        var start = 0
        var end = 0
        for (index in 0 until t.length) {
            val c = t[index]
            if (c.isWhitespace()) {
                if (start != end || !t[start].isWhitespace()) {
                    yield(t.slice(start..end))
                }
                start = index+1
                end = index+1
            } else {
                end = index
            }
        }
        if (start != end || !t[end].isWhitespace()) {
            yield(t.slice(start..end))
        } else if (!t[end].isWhitespace()) {
            yield(t[end].toString())
        }
    }
}

fun String.wordSeqBackwards(): Sequence<String> {
    val t = this
    return buildSequence {
        var start = t.length-1
        var end = t.length-1
        for (it in t.length-1 downTo 0) {
            val c = t[it]
            if (c.isWhitespace()) {
                if (start != end || !t[start].isWhitespace()) {
                    yield(t.slice(start..end))
                }
                start = it-1
                end = it-1
            } else {
                start = it
            }
        }
        if (start != -1 && (start != end || !t[start].isWhitespace())) {
            yield(t.slice(start..end))
        } else if (start == -1 && end > -1 && !t[end].isWhitespace()) {
            yield(t[end].toString())
        }
    }
}
