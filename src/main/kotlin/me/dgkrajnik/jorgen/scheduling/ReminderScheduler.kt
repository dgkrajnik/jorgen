package me.dgkrajnik.jorgen.scheduling

import me.dgkrajnik.jorgen.Reminder
import net.dv8tion.jda.core.JDA
import net.dv8tion.jda.core.entities.User
import org.quartz.*
import org.quartz.CronScheduleBuilder.cronSchedule
import org.quartz.JobBuilder.newJob
import org.quartz.SimpleScheduleBuilder.simpleSchedule
import org.quartz.TriggerBuilder.newTrigger
import org.quartz.impl.matchers.GroupMatcher
import java.security.SecureRandom
import java.sql.Date
import java.time.DayOfWeek
import java.time.Instant
import java.time.format.TextStyle
import java.time.temporal.ChronoField
import java.util.*

class ReminderScheduler(private val scheduler: Scheduler, jda: JDA) {
    init {
        scheduler.context["JDA"] = jda
    }

    fun scheduleReminder(reminder: Reminder) {
        val id = "${SecureRandom().nextInt(1024)}"
        reminder.run {
            val job = newJob(ReminderJob::class.java)
                    .withIdentity(id, "${user.idLong}")
                    .usingJobData("user", user.idLong)
                    .usingJobData("reminder", this.reminder)
                    .usingJobData("start", this.start.toString())
                    .usingJobData("interval", this.recurrencePeriod?.interval?.toString())
                    .usingJobData("weekdays", this.recurrencePeriod?.weekdays?.joinToString(separator = ", ") { it.getDisplayName(TextStyle.FULL, Locale.getDefault()) })
                    .usingJobData("until", this.until?.toString())
                    .build()

            val triggerBuilder: TriggerBuilder<Trigger> = newTrigger()
                    .withIdentity(id, "${user.idLong}")
                    .startAt(Date.from(start))
            val trigger = when {
                recurrencePeriod?.interval != null -> {
                    val tb2 = triggerBuilder.withSchedule(simpleSchedule()
                            .withIntervalInMilliseconds(recurrencePeriod.interval.toMillis()).repeatForever()
                    )
                    tb2.let { tb -> until?.let { tb.endAt(Date.from(it)) } ?: tb }.build()
                }
                recurrencePeriod?.weekdays != null -> {
                    val tb2 = triggerBuilder.withSchedule(cronSchedule(makeCron(start, recurrencePeriod.weekdays)))
                    tb2.let { tb -> until?.let { tb.endAt(Date.from(it)) } ?: tb }.build()
                }
                else -> triggerBuilder.build()
            }

            scheduler.scheduleJob(job, trigger)
        }
    }

    fun listUsersReminders(user: User): List<String> {
        return scheduler.getJobKeys(GroupMatcher.groupEquals("${user.idLong}")).map {
            Pair(scheduler.getJobDetail(it), scheduler.getTriggersOfJob(it).single())
        }.sortedBy { it.second.nextFireTime }.map{ jobToMessage(it.first) }
    }

    fun cancelByName(reminderName: String, onBehalfOf: User): Boolean {
        val job = scheduler.getJobDetail(JobKey.jobKey(reminderName, "${onBehalfOf.idLong}")) ?: return false
        scheduler.deleteJob(job.key)
        return true
    }

    private fun jobToMessage(job: JobDetail): String {
        val username = (scheduler.context["JDA"] as JDA).getUserById(job.jobDataMap["user"] as Long).name
        val sb = StringBuilder()
        sb.append("${job.key.name}: Reminder for $username at ${job.jobDataMap["start"]}")
        job.jobDataMap["interval"]?.also {
            sb.append(", repeating every $it")
        }
        job.jobDataMap["weekdays"]?.also {
            sb.append(", repeating every $it")
        }
        job.jobDataMap["until"]?.also {
            sb.append(" until $it")
        }
        sb.append(". The reminder will be \"${job.jobDataMap["reminder"]}\"")
        return sb.toString()
    }

    private fun makeCron(start: Instant, weekdays: List<DayOfWeek>): String {
        val rec = weekdays.joinToString(",")
        val sec = start.get(ChronoField.SECOND_OF_MINUTE)
        val min = start.get(ChronoField.MINUTE_OF_HOUR)
        val hr = start.get(ChronoField.HOUR_OF_DAY)
        return "$sec $min $hr * * $rec *"
    }
}
