package me.dgkrajnik.jorgen.scheduling

import me.dgkrajnik.jorgen.logger
import net.dv8tion.jda.core.JDA
import net.dv8tion.jda.core.entities.Message
import org.quartz.Job
import org.quartz.JobExecutionContext
import org.quartz.JobExecutionException
import java.util.function.Consumer

class ReminderJob : Job {
    private val LOG by logger()
    private val messageLoggerCallback = Consumer<Message> {LOG.info("REMINDERJOB: ${it.contentRaw}")}

    @Throws(JobExecutionException::class)
    override fun execute(context: JobExecutionContext) {
        LOG.info("Started a job")
        val jda: JDA = context.scheduler.context["JDA"] as JDA
        val user: Long = context.mergedJobDataMap["user"] as Long
        val reminder: String = context.mergedJobDataMap["reminder"] as String
        jda.getUserById(user).openPrivateChannel().complete(true).sendMessage("Reminder for you: $reminder").queue(messageLoggerCallback)
    }
}
