package me.dgkrajnik.jorgen

import com.joestelmach.natty.Parser
import me.dgkrajnik.jorgen.parsing.*
import me.dgkrajnik.jorgen.scheduling.ReminderJob
import me.dgkrajnik.jorgen.scheduling.ReminderScheduler
import net.dv8tion.jda.core.JDA
import net.dv8tion.jda.core.entities.*
import net.dv8tion.jda.core.events.Event
import net.dv8tion.jda.core.events.ReadyEvent
import net.dv8tion.jda.core.events.message.MessageReceivedEvent
import net.dv8tion.jda.core.hooks.EventListener
import org.quartz.CronScheduleBuilder.cronSchedule
import org.quartz.JobBuilder.newJob
import org.quartz.Scheduler
import org.quartz.SimpleScheduleBuilder.simpleSchedule
import org.quartz.Trigger
import org.quartz.TriggerBuilder
import org.quartz.TriggerBuilder.newTrigger
import java.io.File
import java.security.SecureRandom
import java.sql.Date
import java.time.Duration
import java.time.Instant
import java.time.format.TextStyle
import java.time.temporal.ChronoField
import java.time.temporal.ChronoUnit
import java.util.*
import java.util.concurrent.TimeUnit
import java.util.function.Consumer

class EventHandler(private var botname: String, private val jda: JDA, scheduler: Scheduler): EventListener {
    private val LOG by logger()

    private val messageLoggerCallback = Consumer<Message> {LOG.info("$botname: ${it.contentRaw}")}
    private val parser = Parser()

    // Maps user IDs to parsed unaccepted reminders and the time they were added.
    private val pendingReminders: MutableMap<Long, Pair<Reminder, Instant>> = mutableMapOf()

    private val reminderScheduler = ReminderScheduler(scheduler, jda)

    fun changeBotName(newname: String) {
        botname = newname
        jda.selfUser.manager.setName(botname).queue()
    }

    fun changeBotAvatar(newavatar: File) {
        if (!newavatar.exists()) throw IllegalArgumentException("Avatar file does not exist.")
        val avatar = Icon.from(newavatar)
        jda.selfUser.manager.setAvatar(avatar).queue()
    }

    override fun onEvent(event: Event?) {
        when (event) {
            is ReadyEvent -> {
                LOG.info("We're alive!")
            }
            is MessageReceivedEvent -> {
                handleMessage(event)
            }
        }
    }

    private fun handleMessage(event: MessageReceivedEvent) {
        if (event.author.isBot) return

        val content = event.message.contentRaw
        when {
            content.startsWith("!help") -> {
                printHelp(event.channel)
            }
            content.startsWith("!remind ") -> {
                remind(content.removePrefix("!remind "), event.channel, event.guild, event.author)
            }
            content.startsWith("!remindme ") -> {
                remind(content.removePrefix("!remindme "), event.channel, event.guild, event.author, userKnown = true)
            }
            content.startsWith("!accept") -> {
                accept(event.channel, event.author)
            }
            content.startsWith("!listreminders") -> {
                listReminders(event.author)
            }
            content.startsWith("!cancel ") -> {
                cancelReminder(content.removePrefix("!cancel "), event.channel, event.author)
            }
        }
    }

    private fun printHelp(channel: MessageChannel) {
        channel.sendMessage("!remind <PERSON> <REMINDER> AT <TIME> (EVERY <INTERVAL> (UNTIL <TIME>))\n" +
                            "!remindme <REMINDER> AT <TIME> (EVERY <INTERVAL> (UNTIL <TIME>))\n" +
                            "!listreminders\n" +
                            "!cancel <REMINDER_ID>\n").queue(messageLoggerCallback)
    }

    private fun remind(message: String, channel: MessageChannel, guild: Guild?, author: User, userKnown: Boolean = false) {
        try {
            val reminderreq = parseReminder(message.removePrefix("!remind "), userKnown = userKnown)
            LOG.debug("Parsed out $reminderreq")
            confirmReminder(channel, guild, author, reminderreq, isForAnotherUser = !userKnown)
        } catch (e: RemindParseError) {
            abort("Sorry, I couldn't understand that! Here's a hint from the other part of my brain: ${e.hint}", e, channel)
        }
    }

    private fun accept(channel: MessageChannel, author: User) {
        pendingReminders.remove(author.idLong)?.apply {
            val delay = Duration.between(second, getMonotonicInstantNow())
            if (delay <= Duration.ofMinutes(5)) {
                enableReminder(first)
                channel.sendMessage("Reminder \"${first.reminder}\" enabled.").queue(messageLoggerCallback)
            }
        }
    }

    private fun listReminders(author: User) {
        author.openPrivateChannel().complete().sendMessage(reminderScheduler.listUsersReminders(author).joinToString("\n")).queue(messageLoggerCallback)
    }

    private fun cancelReminder(reminderName: String, channel: MessageChannel, author: User) {
        if (!reminderScheduler.cancelByName(reminderName, author)) {
            abort("Sorry, I couldn't find that reminder ID.", IllegalArgumentException("Invalid reminder ID."), channel)
        } else {
            channel.sendMessage("Cancelled reminder $reminderName for ${author.name}.").queue(messageLoggerCallback)
        }
    }

    private fun enableReminder(reminder: Reminder) = reminderScheduler.scheduleReminder(reminder)

    private fun makeReminder(channel: MessageChannel, guild: Guild?, author: User, parse: ReminderParse): Reminder {
        val user = parseUser(channel, guild, author, parse)
        val reminder = parse.reminder
        val start = parseStart(parse.start, channel)
        val recurrencePeriod = parse.recurrencePeriod?.let { parseRecurrence(it, channel) }
        val until = parse.until?.let { parseUntil(it, channel) }
        return Reminder(user, reminder, start, recurrencePeriod, until)
    }

    private fun confirmReminder(channel: MessageChannel, guild: Guild?, author: User, parse: ReminderParse, isForAnotherUser: Boolean = false) {
        val reminder = makeReminder(channel, guild, author, parse)
        if (reminder.user.idLong == jda.selfUser.idLong) {
            abort("Nice try buddy.", Exception(), channel)
        }
        if (reminder.start < Instant.now()) {
            abort("Sorry, you can't set a reminder for a time before now.", Exception(), channel)
        }
        if (reminder.recurrencePeriod?.interval != null && reminder.recurrencePeriod.interval < Duration.ofSeconds(10)) {
            abort("Sorry, due to rate limiting I can't guarantee delivery of reminders that often.", Exception(), channel)
        }
        if (isForAnotherUser) {
            val millis = reminder.recurrencePeriod?.interval?.toMillis()
            if (millis != null && millis < 120*1000) {
                abort("Sorry, that period of time is too short use on another user.", Exception(), channel)
            }
        }
        addPendingReminder(reminder, author)
        channel.sendMessage(reminderMessage(reminder)).queue(messageLoggerCallback)
    }

    private fun addPendingReminder(reminder: Reminder, author: User) {
        pendingReminders[author.idLong] = Pair(reminder, getMonotonicInstantNow())
    }

    private fun parseRecurrence(recurrence: String, channel: MessageChannel): RecurrenceIncrement {
        try {
            return recurrenceParser(recurrence, Instant.now())
        } catch(e: RecurParseError) {
            abort("Sorry, I couldn't parse your recurrence period.", e, channel)
        }
    }

    private fun parseStart(start: String, channel: MessageChannel): Instant {
        return parser.parse(start).firstOrNull()?.dates?.firstOrNull()?.toInstant()
                ?: abort("Sorry, I couldn't parse your 'start' time.", IllegalArgumentException("Unable to parse '$start'"), channel)
    }

    private fun parseUntil(until: String, channel: MessageChannel): Instant {
        return parser.parse(until).firstOrNull()?.dates?.firstOrNull()?.toInstant()
                ?: abort("Sorry, I couldn't parse your 'until' time.", IllegalArgumentException("Unable to parse '$until'"), channel)
    }

    private fun parseUser(channel: MessageChannel, guild: Guild?, author: User, parse: ReminderParse): User {
        val user = if (parse.user == null) {
            author
        } else {
            if (guild == null) {
                null
            } else if (parse.user.startsWith("<@!") && parse.user.endsWith(">")) {
                parse.user.substringAfter("!").substringBefore(">").toLongOrNull()?.let { guild.getMemberById(it)?.user }
            } else {
                guild.getMembersByEffectiveName(parse.user, true).getOrNull(0)?.user
            }
        }

        return user ?: abort("Sorry, I couldn't find the user you asked for.", IllegalArgumentException("User not found"), channel)
    }

    private fun reminderMessage(reminder: Reminder): String {
        return reminder.run {
            val sb = StringBuilder()
            sb.append("Okay, I'll set a reminder for ${user.name} at $start")
            recurrencePeriod?.apply {
                sb.append(", repeating ")
                when {
                    interval != null -> sb.append("every $interval")
                    weekdays != null -> {
                        sb.append("every ")
                        sb.append (weekdays.joinToString(separator = ", ") { it.getDisplayName(TextStyle.FULL, Locale.getDefault()) })
                    }
                    else -> sb.append("every ERROR")
                }

                until?.also { sb.append(" until $it") }
            }
            sb.append(". The reminder will be \"${reminder.reminder}\"")
            sb.append("\nIf that's okay, enter !accept.")
        }.toString()
    }

    private fun abort(message: String, exception: Exception, channel: MessageChannel): Nothing {
        channel.sendMessage(message).queue(messageLoggerCallback)
        throw exception
    }

    // It doesn't appear that instant.now is necessarily monotonic, but nanoTime *is* if the platform supports it.
    private fun getMonotonicInstantNow(): Instant = Instant.ofEpochMilli(TimeUnit.NANOSECONDS.toMillis(System.nanoTime()))
}

data class Reminder(
        val user: User,
        val reminder: String,
        val start: Instant,
        val recurrencePeriod: RecurrenceIncrement?,
        val until: Instant?
)
