package me.dgkrajnik.jorgen.parsing

import me.dgkrajnik.jorgen.wordSeq
import java.time.*
import java.time.temporal.ChronoUnit
import java.time.temporal.TemporalAdjusters
import kotlin.math.roundToLong

fun recurrenceParser(recurrence: String, baseTime: Instant): RecurrenceIncrement {
    return customRecurrenceParser(recurrence, baseTime)
}

// Natty doesn't really parse recurrences, and I can't find any libraries
// online to do it neatly, so I guess I'll just have to make it myself.
// The existence of this code represents me being The Big Sad.
internal fun customRecurrenceParser(recurrence: String, baseTime: Instant): RecurrenceIncrement {
    var duration = Duration.ZERO
    val weekdays = mutableListOf<DayOfWeek>()
    var unitCount: Double? = null
    var unitTokenCount = 0

    recurrence.wordSeq().forEach {
        val word = it.replace(",", "")
        val token = RecurTokenCategory.categorise(word)
        var unit: RecurUnitType? = null

        when (token) {
            is RecurTokenCategory.WEEKDAY -> weekdays.add(token.weekday.toDayOfWeek())
            is RecurTokenCategory.UNIT -> unit = token.unit
            is RecurTokenCategory.WORD -> run {
                if (word.toLowerCase() in IGNOREDWORDS) return@run
                val iuc = word.toDoubleOrNull()
                val (subCount, subUnit) = iuc?.let{ Pair(iuc, null) } ?: run {
                    val (number, subToken) = RecurTokenCategory.splitByNumber(word.toLowerCase())
                    val realNumber = number ?: throw RecurParseError("Found invalid number while trying to parse a meshed word $number:$subToken", "Did you check that all numbers are valid?")
                    if (subToken !is RecurTokenCategory.UNIT) throw RecurParseError("Found invalid unit while trying to parse a meshed word $number:$subToken", "Did you check that all unspaced-unit values are valid?")
                    Pair(realNumber, subToken.unit)
                }
                if (unitCount != null) throw RecurParseError("Found numbers without matching units.", "Did you check that all numbers have an accompanying time unit?")
                unitCount = subCount
                unit = subUnit
            }
        }

        unit?.run {
            if (unitCount == null && unitTokenCount > 1) throw RecurParseError("Found multiple units, with some units not having matching numbers.", "Did you check that each time unit has an accompanying number?")
            if (unitCount == null) { unitCount = 1.0 }
            if (unitCount != null && unitCount as Double <= 0) throw RecurParseError("Recurrence values need to be > 0", "Did you specify a recurrence unit that's <= 0?")
            unitTokenCount += 1
            duration += handleUnit(this, unitCount as Double)
            unit = null
            unitCount = null
        }

        if (weekdays.size > 1 && unitTokenCount > 1) {
            throw RecurParseError("Found both weekdays and units in a recurrence specification", "Did you mix your recurrences?")
        }
    }

    return if (weekdays.size >= 1) {
        val nextDay = weekdays.map {
            baseTime.atZone(ZoneId.of("UTC")).with(TemporalAdjusters.nextOrSame(it)).toInstant()
        }.min()!!
        RecurrenceIncrement(baseTime, nextDay, null, weekdays)
    } else {
        RecurrenceIncrement(baseTime, baseTime.plus(duration), duration, null)
    }

}

internal fun handleUnit(tokenType: RecurUnitType, unitCount: Double): Duration {
    val millisInUnit = Duration.of(1, tokenType.toChrono()).toMillis()
    val millisInDuration = millisInUnit * unitCount
    return Duration.of(millisInDuration.roundToLong(), ChronoUnit.MILLIS)
}

data class RecurParseError(override val message: String, val hint: String) : Exception(message)

data class RecurrenceIncrement(val baseTime: Instant, val nextTime: Instant, val interval: Duration?, val weekdays: List<DayOfWeek>?) {
    init {
        require((interval == null && weekdays != null && weekdays.isNotEmpty()) || (interval != null && weekdays == null))
    }
}

internal sealed class RecurTokenCategory {
    data class UNIT(val unit: RecurUnitType) : RecurTokenCategory()
    data class WEEKDAY(val weekday: RecurDayType) : RecurTokenCategory()
    data class WORD(val word: String) : RecurTokenCategory()

    companion object {
        fun categorise(token: String): RecurTokenCategory {
            return RecurUnitType.typify(token)?.let{ UNIT(it) } ?: RecurDayType.typify(token)?.let{ WEEKDAY(it) } ?: WORD(token)
        }

        fun splitByNumber(token: String): Pair<Double?, RecurTokenCategory> {
            val (numberstring, word) = token.partition { it.isDigit() || it == '.' || it == ',' }
            val number = numberstring.toDoubleOrNull()
            val wordtoken = categorise(word)
            return Pair(number, wordtoken)
        }
    }
}

internal enum class RecurDayType {
    MONDAY { override fun toDayOfWeek() = DayOfWeek.MONDAY },
    TUESDAY { override fun toDayOfWeek() = DayOfWeek.TUESDAY },
    WEDNESDAY { override fun toDayOfWeek() = DayOfWeek.WEDNESDAY },
    THURSDAY { override fun toDayOfWeek() = DayOfWeek.THURSDAY },
    FRIDAY { override fun toDayOfWeek() = DayOfWeek.FRIDAY },
    SATURDAY { override fun toDayOfWeek() = DayOfWeek.SATURDAY },
    SUNDAY { override fun toDayOfWeek() = DayOfWeek.SUNDAY };

    abstract fun toDayOfWeek(): DayOfWeek

    companion object {
        fun typify(token: String): RecurDayType? {
            return when (token) {
                in MONDAYSET -> MONDAY
                in TUESDAYSET -> TUESDAY
                in WEDNESDAYSET -> WEDNESDAY
                in THURSDAYSET -> THURSDAY
                in FRIDAYSET -> FRIDAY
                in SATURDAYSET -> SATURDAY
                in SUNDAYSET -> SUNDAY
                else -> null
            }
        }
    }
}

internal enum class RecurUnitType {
    YEARS { override fun toChrono() = ChronoUnit.YEARS },
    MONTHS { override fun toChrono() = ChronoUnit.MONTHS },
    WEEKS { override fun toChrono() = ChronoUnit.WEEKS },
    DAYS { override fun toChrono() = ChronoUnit.DAYS },
    HOURS { override fun toChrono() = ChronoUnit.HOURS },
    MINUTES { override fun toChrono() = ChronoUnit.MINUTES },
    SECONDS { override fun toChrono() = ChronoUnit.SECONDS };

    abstract fun toChrono(): ChronoUnit

    companion object {
        fun typify(token: String): RecurUnitType? {
            return when (token) {
                in YEARSET -> YEARS
                in MONTHSET -> MONTHS
                in WEEKSET -> WEEKS
                in DAYSET -> DAYS
                in HOURSET -> HOURS
                in MINUTESET -> MINUTES
                in SECONDSET -> SECONDS
                else -> null
            }
        }
    }
}

// TODO: Fix these.
//internal val YEARSET = setOf("year", "years", "yr", "yrs")
//internal val MONTHSET = setOf("month", "months", "mth", "mths")
//internal val WEEKSET = setOf("week", "weeks", "wk", "wks")
internal val YEARSET = emptySet<String>()
internal val MONTHSET = emptySet<String>()
internal val WEEKSET = emptySet<String>()
internal val DAYSET = setOf("day", "days", "d")
internal val HOURSET = setOf("hour", "hours", "hr", "hrs", "h")
internal val MINUTESET = setOf("minute", "minutes", "min", "mins", "m")
internal val SECONDSET = setOf("second", "seconds", "sec", "secs", "s")
internal val MONDAYSET = setOf("monday", "mon")
internal val TUESDAYSET = setOf("tuesday", "tue")
internal val WEDNESDAYSET = setOf("wednesday", "wed")
internal val THURSDAYSET = setOf("thursday", "thu")
internal val FRIDAYSET = setOf("friday", "fri")
internal val SATURDAYSET = setOf("saturday", "sat")
internal val SUNDAYSET = setOf("sunday", "sun")
internal val IGNOREDWORDS = setOf("and")
