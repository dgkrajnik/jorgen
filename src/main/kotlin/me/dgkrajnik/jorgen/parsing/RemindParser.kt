package me.dgkrajnik.jorgen.parsing

import me.dgkrajnik.jorgen.wordSeqBackwards

// Super formal grammar: remind <PERSON> <TEXT> [ON/AT/AFTER] <TIME> (EVERY <PERIOD> (UNTIL <TIME>))

fun parseReminder(message: String, userKnown: Boolean = false): ReminderParse {
    // We know these will only be assigned once but Kotlin can't prove it so we need them to be var.
    var user: String? = null
    val reminder: String
    var start: String = ""
    var recurrencePeriod: String? = null
    var until: String? = null
    val builder = mutableListOf<String>()
    // We iterate in reverse order because otherwise this grammar would require infinite look-ahead.
    var state = ParserState.UNTIL
    message.wordSeqBackwards().forEach {
        val tokenclass = classifyToken(it)
        when (state) {
            ParserState.UNTIL -> {
                when (tokenclass) {
                    TokenType.UNTILSTART -> {
                        until = builder.asReversed().joinToString(" ")
                        builder.clear()
                        state = state.pred()
                    }
                    TokenType.RECURRENCESTART -> {
                        recurrencePeriod = builder.asReversed().joinToString(" ")
                        builder.clear()
                        state = state.pred().pred()
                    }
                    TokenType.TIMESTART -> {
                        if (builder.size < 1) throw RemindParseError("Found TIMESTART without finding a time", "Did you forget to enter a time?", state.toString(), builder)
                        start = builder.asReversed().joinToString(" ")
                        builder.clear()
                        state = state.pred().pred().pred()
                    }
                    else -> builder.add(it)
                }
            }
            ParserState.RECURRENCE -> {
                when (tokenclass) {
                    TokenType.UNTILSTART -> throw RemindParseError("Found UNTILSTART while parsing a recurrence", "Did you forget EVERY, or AT/AFTER/ON?", state.toString(), builder)
                    TokenType.RECURRENCESTART -> {
                        recurrencePeriod = builder.asReversed().joinToString(" ")
                        builder.clear()
                        state = state.pred()
                    }
                    TokenType.TIMESTART -> {
                        if (until != null) throw RemindParseError("Found TIMESTART while expecting EVERYSTART", "Did you forget EVERY?", state.toString(), builder)
                        start = builder.asReversed().joinToString(" ")
                        builder.clear()
                        state = state.pred().pred()
                    }
                    else -> builder.add(it)
                }
            }
            ParserState.START -> {
                when (tokenclass) {
                    TokenType.UNTILSTART -> throw RemindParseError("Found UNTILSTART while parsing a start time", "Did you forget AT/AFTER/ON?", state.toString(), builder)
                    TokenType.RECURRENCESTART -> throw RemindParseError("Found RECURRENCESTART while parsing a start time", "Did you forget AT/AFTER/ON?", state.toString(), builder)
                    TokenType.TIMESTART -> {
                        start = builder.asReversed().joinToString(" ")
                        builder.clear()
                        state = state.pred()
                    }
                    else -> builder.add(it)
                }
            }
            ParserState.REMINDER -> builder.add(it)
            ParserState.USER -> throw RemindParseError("!!!SOMEHOW HIT USER!!!", "Pray to your god, if you have one.", state.toString(), builder)
        }
    }

    when (state) {
        ParserState.UNTIL -> throw RemindParseError("Found start of string while expecting UNTILSTART", "Did you forget AT/AFTER/ON?", state.toString(), builder)
        ParserState.RECURRENCE -> throw RemindParseError("Found start of string while expecting RECURRENCESTART", "Did you forget AT/AFTER/ON?", state.toString(), builder)
        ParserState.START -> throw RemindParseError("Found start of string while expecting TIMESTART", "Did you forget AT/AFTER/ON?", state.toString(), builder)
        ParserState.REMINDER -> {
            if (!userKnown && builder.size == 1) {throw RemindParseError("Found start of string while expecting user", "Did you forget a user?", state.toString(), builder)
            }
            else if (builder.size == 0) {throw RemindParseError("Found empty tokenisation", "Did you forget to... type... anything?", state.toString(), builder)
            }
        }
        ParserState.USER -> throw RemindParseError("!!!SOMEHOW HIT USER!!!", "Pray to your god, if you have one.", state.toString(), builder)
    }

    if (userKnown) {
        reminder = builder.asReversed().joinToString(" ")
    } else {
        reminder = builder.asReversed().slice(1 until builder.size).joinToString(" ")
        user = builder.asReversed()[0]
    }

    if (start.isBlank()) {
        throw RemindParseError("!!!START SOMEHOW EMPTY!!!", "Consult your local spiritual leader for aid.", state.toString(), builder)
    }

    return ReminderParse(user, reminder, start, recurrencePeriod, until)
}

data class ReminderParse(val user: String?, val reminder: String, val start: String, val recurrencePeriod: String?, val until: String?) {
    init {
        if (recurrencePeriod == null && until != null) {
            throw IllegalArgumentException("Can't have an until without a recurrence period.")
        }
    }
}

data class RemindParseError(override val message: String, val hint: String, val state: String, val stack: List<String>): Exception(message)

internal fun classifyToken(token: String): TokenType {
    return when (token.toLowerCase()) {
        "after" -> TokenType.TIMESTART
        "on"    -> TokenType.TIMESTART
        "at"    -> TokenType.TIMESTART
        "every" -> TokenType.RECURRENCESTART
        "until" -> TokenType.UNTILSTART
        else    -> TokenType.WORD
    }
}

internal enum class TokenType {
    WORD,
    TIMESTART,
    RECURRENCESTART,
    UNTILSTART
}

internal enum class ParserState {
    USER {
        override fun succ() = REMINDER
        override fun pred() = USER
    },
    REMINDER {
        override fun succ() = START
        override fun pred() = USER
    },
    START {
        override fun succ() = RECURRENCE
        override fun pred() = REMINDER
    },
    RECURRENCE {
        override fun succ() = UNTIL
        override fun pred() = START
    },
    UNTIL {
        override fun succ() = UNTIL
        override fun pred() = RECURRENCE
    };

    abstract fun succ(): ParserState
    abstract fun pred(): ParserState
}