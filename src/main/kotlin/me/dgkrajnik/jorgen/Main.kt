package me.dgkrajnik.jorgen

import com.typesafe.config.ConfigFactory
import net.dv8tion.jda.core.AccountType
import net.dv8tion.jda.core.JDABuilder
import org.apache.logging.log4j.LogManager
import org.quartz.impl.StdSchedulerFactory
import sun.misc.Signal
import java.nio.file.Paths
import java.sql.Connection
import java.sql.DriverManager

fun main(args: Array<String>) {
    val LOG = LogManager.getLogger()

    val conf = ConfigFactory.parseFile(Paths.get("application.conf").toFile())
    val token = conf.getString("jda.token")
    val botname = conf.getString("jorgen.botname")

    LOG.info("Hello!")

    val scheduler = StdSchedulerFactory().scheduler

    val jda = JDABuilder(AccountType.BOT)
            .setToken(token)
            .build()
    val eventHandler = EventHandler(botname, jda, scheduler)
    jda.addEventListener(eventHandler)

    // Make sure the bot is named correctly at startup.
    jda.awaitReady()
    eventHandler.changeBotName(botname)

    scheduler.context["JDA"] = jda

    scheduler.start()

    // Signal handlers to gracefully shut down JDA & Quartz.
    Signal.handle(Signal("TERM")) {LOG.info("Goodbye!"); jda.shutdown(); scheduler.shutdown()}
    Signal.handle(Signal("INT")) {LOG.info("Goodbye!"); jda.shutdown(); scheduler.shutdown()}
    Runtime.getRuntime().addShutdownHook(Thread(Runnable {jda.shutdown(); scheduler.shutdown()}))
}
