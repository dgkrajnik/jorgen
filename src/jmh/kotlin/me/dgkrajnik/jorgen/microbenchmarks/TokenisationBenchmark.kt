package me.dgkrajnik.jorgen.microbenchmarks

import me.dgkrajnik.jorgen.wordSeq
import me.dgkrajnik.jorgen.wordSeqBackwards
import org.openjdk.jmh.annotations.*

@State(Scope.Benchmark)
@Warmup(iterations=1, time=5)
@Measurement(iterations=1, time=5)
@Threads(1)
@Fork(2, warmups=0)
open class TokenisationBenchmark {
    val testString = "kobayashi                don't forget the integration tests    on next tuesday every 30 minutes until wednesday"
    @Benchmark
    fun naiveBenchmark() {
        var count = 0
        testString.split("\\s+".toRegex()).forEach {
            count += 1
        }
    }

    @Benchmark
    fun sequenceBenchmark() {
        var count = 0
        testString.wordSeq().forEach {
            count += 1
        }
    }

    @Benchmark
    fun naiveBackwardsBenchmark() {
        var count = 0
        testString.split("\\s+".toRegex()).asReversed().forEach {
            count += 1
        }
    }

    @Benchmark
    fun sequenceBackwardsBenchmark() {
        var count = 0
        testString.wordSeqBackwards().forEach {
            count += 1
        }
    }
}
