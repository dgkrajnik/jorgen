package me.dgkrajnik.jorgen.tests

import me.dgkrajnik.jorgen.parsing.RecurParseError
import me.dgkrajnik.jorgen.parsing.recurrenceParser
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.CsvSource
import java.time.DayOfWeek
import java.time.Duration
import java.time.Instant
import java.time.ZoneId
import java.time.temporal.ChronoUnit
import java.time.temporal.TemporalAdjusters
import kotlin.test.assertEquals
import kotlin.test.assertNotNull
import kotlin.test.assertNull

class RecurrenceParserTests {
    val baseTime = Instant.ofEpochSecond(15000000)

    @ParameterizedTest
    @CsvSource(
            "1, hour, 3600000",
            "2, hours, 7200000",
            "2, hrs, 7200000",
            "6, hr, 21600000",
            "10, hour, 36000000",
            "1.5, hours, 5400000",
            "12.2, hrs, 43920000",
            "0.25, hr, 900000",
            "1, minute, 60000",
            "2, minutes, 120000",
            "2, mins, 120000",
            "6, min, 360000",
            "10, m, 600000",
            "1.5, min, 90000",
            "12.2, mins, 732000",
            "0.25, min, 15000",
            "1, second, 1000",
            "2, seconds, 2000",
            "2, secs, 2000",
            "6, sec, 6000",
            "10, sec, 10000",
            "1.5, secs, 1500",
            "12.2, secs, 12200",
            "0.25, sec, 250"
    )
    internal fun `basics unit types`(count: Double, type: String, millis: Long) {
        val parsed = recurrenceParser("$count $type", baseTime)
        val gap = Duration.of(millis, ChronoUnit.MILLIS)
        assertEquals(baseTime, parsed.baseTime)
        assertEquals(baseTime + gap, parsed.nextTime)
        assertNull(parsed.weekdays)
        assertEquals(gap, parsed.interval)
    }

    @ParameterizedTest
    @CsvSource(
            "1, hour, 3600000",
            "2, hours, 7200000",
            "2, hrs, 7200000",
            "6, hr, 21600000",
            "10, hour, 36000000",
            "1.5, hours, 5400000",
            "12.2, hrs, 43920000",
            "0.25, hr, 900000",
            "1, minute, 60000",
            "2, minutes, 120000",
            "2, mins, 120000",
            "6, min, 360000",
            "10, m, 600000",
            "1.5, min, 90000",
            "12.2, mins, 732000",
            "0.25, min, 15000",
            "1, second, 1000",
            "2, seconds, 2000",
            "2, secs, 2000",
            "6, sec, 6000",
            "10, sec, 10000",
            "1.5, secs, 1500",
            "12.2, secs, 12200",
            "0.25, sec, 250"
    )
    internal fun `meshed unit types`(count: Double, type: String, millis: Long) {
        val parsed = recurrenceParser("$count$type", baseTime)
        val gap = Duration.of(millis, ChronoUnit.MILLIS)
        assertEquals(baseTime, parsed.baseTime)
        assertEquals(baseTime + gap, parsed.nextTime)
        assertNull(parsed.weekdays)
        assertEquals(gap, parsed.interval)
    }

    @ParameterizedTest
    @CsvSource(
            "1, hour, 3600000, 2, hrs, 7200000",
            "6, hr, 21600000, 10, hour, 36000000",
            "6, sec, 6000, 10, sec, 10000",
            "1.5, hours, 5400000, 1.5, min, 90000",
            "1, second, 1000, 0.25, hr, 900000",
            "1, minute, 60000, 2, hours, 7200000",
            "2, minutes, 120000, 6, min, 360000",
            "10, min, 600000, 12.2, mins, 732000",
            "0.25, min, 15000, 2, mins, 120000",
            "2, secs, 2000, 1.5, secs, 1500",
            "12.2, hrs, 43920000, 2, seconds, 2000",
            "12.2, secs, 12200, 0.25, sec, 250"
    )
    internal fun `mixed unit types`(counta: Double, typea: String, millisa: Long, countb: Double, typeb: String, millisb: Long) {
        val parsed = recurrenceParser("$counta $typea $countb $typeb", baseTime)
        val gap = Duration.of(millisa+millisb, ChronoUnit.MILLIS)
        assertEquals(baseTime, parsed.baseTime)
        assertEquals(baseTime + gap, parsed.nextTime)
        assertNull(parsed.weekdays)
        assertEquals(gap, parsed.interval)
    }

    @ParameterizedTest
    @CsvSource(
            "1, hour, 3600000, 2, hrs, 7200000",
            "6, hr, 21600000, 10, hour, 36000000",
            "6, sec, 6000, 10, sec, 10000",
            "1.5, hours, 5400000, 1.5, min, 90000",
            "1, second, 1000, 0.25, hr, 900000",
            "1, minute, 60000, 2, hours, 7200000",
            "2, minutes, 120000, 6, min, 360000",
            "10, min, 600000, 12.2, mins, 732000",
            "0.25, min, 15000, 2, mins, 120000",
            "2, secs, 2000, 1.5, secs, 1500",
            "12.2, hrs, 43920000, 2, seconds, 2000",
            "12.2, secs, 12200, 0.25, sec, 250"
    )
    internal fun `mixed unit function words`(counta: Double, typea: String, millisa: Long, countb: Double, typeb: String, millisb: Long) {
        val parsed = recurrenceParser("$counta $typea and $countb $typeb", baseTime)
        val gap = Duration.of(millisa+millisb, ChronoUnit.MILLIS)
        assertEquals(baseTime, parsed.baseTime)
        assertEquals(baseTime + gap, parsed.nextTime)
        assertNull(parsed.weekdays)
        assertEquals(gap, parsed.interval)
    }

    @ParameterizedTest
    @CsvSource(
            "1, hour, 3600000, 2, hrs, 7200000",
            "6, hr, 21600000, 10, hour, 36000000",
            "6, sec, 6000, 10, sec, 10000",
            "1.5, hours, 5400000, 1.5, min, 90000",
            "1, second, 1000, 0.25, hr, 900000",
            "1, minute, 60000, 2, hours, 7200000",
            "2, minutes, 120000, 6, min, 360000",
            "10, min, 600000, 12.2, mins, 732000",
            "0.25, min, 15000, 2, mins, 120000",
            "2, secs, 2000, 1.5, secs, 1500",
            "12.2, hrs, 43920000, 2, seconds, 2000",
            "12.2, secs, 12200, 0.25, sec, 250"
    )
    internal fun `mixed unit commas`(counta: Double, typea: String, millisa: Long, countb: Double, typeb: String, millisb: Long) {
        val parsed = recurrenceParser("$counta $typea, $countb $typeb", baseTime)
        val gap = Duration.of(millisa+millisb, ChronoUnit.MILLIS)
        assertEquals(baseTime, parsed.baseTime)
        assertEquals(baseTime + gap, parsed.nextTime)
        assertNull(parsed.weekdays)
        assertEquals(gap, parsed.interval)
    }

    @ParameterizedTest
    @CsvSource(
            "friday saturday sunday, friday",
            "monday friday sunday, friday",
            "tuesday and wednesday, tuesday",
            "monday tuesday wednesday wednesday wednesday wednesday, tuesday",
            "wed, wednesday",
            "mon, monday",
            "wed mon monday fri friday friday fri, wednesday",
            "sat sun sat, saturday"
    )
    internal fun `basics day types`(daysRequested: String, dayUsed: String) {
        val parsed = recurrenceParser(daysRequested, baseTime)
        val real = baseTime.atZone(ZoneId.of("UTC")).with(TemporalAdjusters.nextOrSame(DayOfWeek.valueOf(dayUsed.toUpperCase()))).toInstant()
        assertEquals(baseTime, parsed.baseTime)
        assertEquals(real, parsed.nextTime)
        assertNotNull(parsed.weekdays)
        assertNull(parsed.interval)
    }

    @ParameterizedTest
    @CsvSource(
            "friday saturday sunday 1 minute",
            "monday friday sunday 1 h",
            "tuesday and wednesday 14 days",
            "monday tuesday 2 seconds wednesday 1 minute wednesday wednesday wednesday",
            "wed day",
            "mon day",
            "wed mon day monday fri friday friday fri",
            "sat sun sat minute"
    )
    internal fun `mixed types`(request: String) {
        try {
            recurrenceParser(request, baseTime)
        } catch (e: RecurParseError) {
            assertEquals("Found both weekdays and units in a recurrence specification", e.message)
        }
    }

    @Test
    internal fun `repeated numerals`() {
        try {
            recurrenceParser("1 1 1 1 1 minutes", baseTime)
        } catch (e: RecurParseError) {
            assertEquals("Found numbers without matching units.", e.message)
        }
    }

    @Test
    internal fun `repeated units`() {
        try {
            recurrenceParser("1 seconds minutes", baseTime)
        } catch (e: RecurParseError) {
            assertEquals("Found multiple units, with some units not having matching numbers.", e.message)
        }
    }
}
