package me.dgkrajnik.jorgen.tests

import me.dgkrajnik.jorgen.parsing.RemindParseError
import me.dgkrajnik.jorgen.parsing.parseReminder
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals
import kotlin.test.assertNull

class RemindParserTests {
    // kobayashi don't forget the integration tests on next tuesday every 3 minutes until next wednesday

    @Test
    fun `simple split all unambiguous`() {
        val user = "kobayashi"
        val reminder = "don't forget the integration tests"
        val start = "next tuesday"
        val recurrencePeriod = "3 minutes"
        val until = "next wednesday"
        val parsed = parseReminder("$user $reminder on $start every $recurrencePeriod until $until")
        assertEquals(user, parsed.user)
        assertEquals(reminder, parsed.reminder)
        assertEquals(start, parsed.start)
        assertEquals(recurrencePeriod, parsed.recurrencePeriod)
        assertEquals(until, parsed.until)
    }

    @Test
    fun `simple split except until unambiguous`() {
        val user = "kobayashi"
        val reminder = "don't forget the integration tests"
        val start = "next tuesday"
        val recurrencePeriod = "30 minutes"
        val parsed = parseReminder("$user $reminder on $start every $recurrencePeriod")
        assertEquals(user, parsed.user)
        assertEquals(reminder, parsed.reminder)
        assertEquals(start, parsed.start)
        assertEquals(recurrencePeriod, parsed.recurrencePeriod)
        assertNull(parsed.until)
    }

    @Test
    fun `simple split except until and recurrence unambiguous`() {
        val user = "kobayashi"
        val reminder = "don't forget the integration tests"
        val start = "next tuesday"
        val parsed = parseReminder("$user $reminder on $start")
        assertEquals(user, parsed.user)
        assertEquals(reminder, parsed.reminder)
        assertEquals(start, parsed.start)
        assertNull(parsed.recurrencePeriod)
    }

    @Test
    fun `simple split missing start unambiguous`() {
        val user = "kobayashi"
        val reminder = "don't forget the integration tests"
        try {
            parseReminder("$user $reminder")
        } catch (e: RemindParseError) {
            assertEquals("Found start of string while expecting UNTILSTART", e.message)
        }
    }

    @Test
    fun `simple split missing timestart token unambiguous`() {
        val user = "kobayashi"
        val reminder = "don't forget the integration tests"
        val start = "next tuesday at 6pm"
        try {
            parseReminder("$user $reminder $start")
        } catch (e: RemindParseError) {
            assertEquals("Found start of string while expecting UNTILSTART", e.message)
        }
    }

    @Test
    fun `simple split only timestart token unambiguous`() {
        val user = "kobayashi"
        val reminder = "don't forget the integration tests"
        try {
            parseReminder("$user $reminder on")
        } catch (e: RemindParseError) {
            assertEquals("Found TIMESTART without finding a time", e.message)
        }
    }

    @Test
    fun `simple split only recurrence token unambiguous`() {
        val user = "kobayashi"
        val reminder = "don't forget the integration tests"
        val start = "next tuesday"
        try {
            parseReminder("$user $reminder on $start every")
        } catch (e: RemindParseError) {
            assertEquals("Found RECURRENCESTART without finding a duration", e.message)
        }
    }

    @Test
    fun `simple split only until token unambiguous`() {
        val user = "kobayashi"
        val reminder = "don't forget the integration tests"
        val start = "next tuesday"
        val recurrencePeriod = "30 minutes"
        try {
            parseReminder("$user $reminder on $start every $recurrencePeriod until")
        } catch (e: RemindParseError) {
            assertEquals("Found UNTILSTART without finding a time", e.message)
        }
    }

    @Test
    fun `ambiguous reminder until all`() {
        val user = "kobayashi"
        val reminder = "don't forget the integration tests until you're done"
        val start = "next tuesday"
        val recurrencePeriod = "30 minutes"
        val until = "next wednesday"
        val parsed = parseReminder("$user $reminder on $start every $recurrencePeriod until $until")
        assertEquals(user, parsed.user)
        assertEquals(reminder, parsed.reminder)
        assertEquals(start, parsed.start)
        assertEquals(recurrencePeriod, parsed.recurrencePeriod)
        assertEquals(until, parsed.until)
    }

    @Test
    fun `duplicate recurrencestart all`() {
        val user = "kobayashi"
        val reminder = "don't forget the integration tests"
        val start = "next tuesday"
        val recurrencePeriod = "30 minutes"
        val until = "next wednesday"
        val parsed = parseReminder("$user $reminder on $start every $recurrencePeriod until $until")
        assertEquals(user, parsed.user)
        assertEquals(reminder, parsed.reminder)
        assertEquals(start, parsed.start)
        assertEquals(recurrencePeriod, parsed.recurrencePeriod)
        assertEquals(until, parsed.until)
    }

    @Test
    fun `untilstart without recurrencestart`() {
        val user = "kobayashi"
        val reminder = "don't forget the integration tests"
        val start = "next tuesday"
        val until = "next wednesday"
        try {
            parseReminder("$user $reminder on $start until $until")
        } catch (e: RemindParseError) {
            assertEquals("Found TIMESTART while expecting EVERYSTART", e.message)
        }
    }

    @Test
    fun `untilstart without recurrencestart or timestart`() {
        val user = "kobayashi"
        val reminder = "don't forget the integration tests"
        val until = "next wednesday"
        try {
            parseReminder("$user $reminder until $until")
        } catch (e: RemindParseError) {
            assertEquals("Found start of string while expecting RECURRENCESTART", e.message)
        }
    }

    @Test
    fun `recurrencestart without timestart`() {
        val user = "kobayashi"
        val reminder = "don't forget the integration tests"
        val recurrencePeriod = "30 minutes"
        try {
            parseReminder("$user $reminder every $recurrencePeriod")
        } catch (e: RemindParseError) {
            assertEquals("Found start of string while expecting TIMESTART", e.message)
        }
    }

    @Test
    fun `no markers`() {
        try {
            parseReminder("sasuga, parser")
        } catch (e: RemindParseError) {
            assertEquals("Found start of string while expecting UNTILSTART", e.message)
        }
    }

    @Test
    fun `not enough tokens for a user and a reminder`() {
        val user = "kobayashi"
        val start = "next tuesday"
        try {
            parseReminder("$user on $start")
        } catch (e: RemindParseError) {
            assertEquals("Found start of string while expecting user", e.message)
        }
    }

    @Test
    fun `just whitespace`() {
        try {
            parseReminder("            ")
        } catch (e: RemindParseError) {
            assertEquals("Found start of string while expecting UNTILSTART", e.message)
        }
    }

    @Test
    fun `literally just nothing, nothing at all`() {
        try {
            parseReminder("")
        } catch (e: RemindParseError) {
            assertEquals("Found start of string while expecting UNTILSTART", e.message)
        }
    }


    @Test
    fun `untilstart during recurrence`() {
        val user = "kobayashi"
        val reminder = "don't forget the integration tests"
        val start = "next tuesday"
        val recurrencePeriod = "until 30 minutes"
        try {
            parseReminder("$user $reminder on $start every $recurrencePeriod")
        } catch (e: RemindParseError) {
            assertEquals("Found UNTILSTART while parsing a recurrence", e.message)
        }
    }

    @Test
    fun `untilstart during start`() {
        val user = "kobayashi"
        val reminder = "don't forget the integration tests"
        val start = "next tuesday until wednesday"
        val recurrencePeriod = "30 minutes"
        try {
            parseReminder("$user $reminder on $start every $recurrencePeriod")
        } catch (e: RemindParseError) {
            assertEquals("Found UNTILSTART while parsing a start time", e.message)
        }

    }

    @Test
    fun `recurrencestart during start`() {
        val user = "kobayashi"
        val reminder = "don't forget the integration tests"
        val start = "next tuesday every wednesday"
        val recurrencePeriod = "30 minutes"
        try {
            parseReminder("$user $reminder on $start every $recurrencePeriod")
        } catch (e: RemindParseError) {
            assertEquals("Found RECURRENCESTART while parsing a start time", e.message)
        }
    }
}
