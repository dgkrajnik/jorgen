package me.dgkrajnik.jorgen.tests

import me.dgkrajnik.jorgen.wordSeq
import me.dgkrajnik.jorgen.wordSeqBackwards
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

class UtilTests {
    val testString = "kobayashi    don't forget the  integration tests on  next tuesday every 30 minutes until                                                            wednesday"
    val single = "3 a b"

    @Test
    fun `simple test all`() {
        val a = testString.split("\\s+".toRegex()).toList()
        val b = testString.wordSeq().toList()
        val c = testString.split("\\s+".toRegex()).asReversed().toList()
        val d = testString.wordSeqBackwards().toList()
        assertEquals(a, b)
        assertEquals(c, d)
    }

    @Test
    fun `test single-char words`() {
        val a = single.split("\\s+".toRegex()).toList()
        val b = single.wordSeq().toList()
        val c = single.split("\\s+".toRegex()).asReversed().toList()
        val d = single.wordSeqBackwards().toList()
        assertEquals(a, b)
        assertEquals(c, d)
    }
}